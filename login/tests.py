from django.test import TestCase, Client
from django.contrib.auth.models import User
from django.urls import resolve
from .views import index, logout_fungsi, masuk
# Create your tests here.
class Login(TestCase):

    def test_page_login(self):
        response = Client().get('/login')
        self.assertEqual(response.status_code,301)
        
    def test_page_logout(self):
        response = Client().get('/logout')
        self.assertEqual(response.status_code,301)

    def test_page_signup(self):
        response = Client().get('/signup')
        self.assertEqual(response.status_code,301)

    def test_login_success(self):
        orang = User.objects.create(username='aa', password='aa')
        response_post = Client().post('/login/', {'username':'aa','password':'aa'})
        self.assertEqual(response_post.status_code, 302)
        
    def test_login_failed(self):
        response_post = Client().post('/login/', {'username':'aa', 'email':'aaa@gmail.com','password':'aa'})
        self.assertEqual(response_post.status_code, 302)

    def test_signup_success(self):
        response_post = Client().post('/signup/', {'username':'abc', 'email':'aaa@gmail.com','password':'aa'})
        self.assertEqual(response_post.status_code, 302)
        jumlah = User.objects.all().count()
        self.assertEqual(1,jumlah)
        
    def test_signup_failed(self):
        user = User.objects.create(username='aa', password='aa')
        response_post = Client().post('/signup/', {'username':'aa', 'email':'aaa@gmail.com','password':'aa'})
        self.assertEqual(response_post.status_code, 302)
        

        


