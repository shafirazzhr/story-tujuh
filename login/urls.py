from django.urls import path

from . import views

app_name = 'login'

urlpatterns = [
    path('signup/', views.index, name='signup'),
    path('logout/', views.logout_fungsi, name="logout"),
    path('login/', views.masuk, name="login")
]
