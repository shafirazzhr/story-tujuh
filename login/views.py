from django.shortcuts import render, redirect
from .forms import signUpForm, loginForm
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User
from django.http import HttpResponse
from django.db import IntegrityError
from django.contrib import messages
from django.http.response import HttpResponseRedirect

# Create your views here.
def index(request):
    form = signUpForm()
    if request.method == "POST":
        try:
            form = signUpForm(request.POST)
            if form.is_valid():
                username = form.cleaned_data['username']
                email = form.cleaned_data['email']
                password = form.cleaned_data['password']
                u = User.objects.create_user(username,email,password)
                u.save()
                messages.add_message(request, messages.SUCCESS, "User berhasil dibuat")
                return redirect('/login')
        except IntegrityError:
            messages.add_message(request, messages.ERROR, 'User dengan username tersebut sudah ada')
            return redirect('/signup')
    return render (request, 'login.html', {'form':form, 'nama':'Daftar'})

def logout_fungsi(request):
    logout(request)
    return redirect("/")

def masuk (request):
    form = loginForm()
    flag = False
    if request.method == "POST":
        form = loginForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data['username']
            password = form.cleaned_data['password']
            user = authenticate(request, username=username, password = password)
            if user is not None:
                login(request, user)
                flag = True
                response = {'form':form,'user':user,'flag':flag}
                return redirect('/')
            else:
                messages.add_message(request, messages.ERROR, "Incorrect username or password")
                return HttpResponseRedirect('/login')
    else:
        response = {'form':form,'flag':flag,'nama':'Masuk'}
        return render(request,'login.html',response)
