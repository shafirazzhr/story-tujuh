$( function() {
    $( ".accordion" ).accordion({
      collapsible: true,
      active: false
    });
} );

function moveUp($item) {
    $before = $item.prev();
    $item.insertBefore($before);
}

function moveDown($item) {
    $after = $item.next();
    $item.insertAfter($after);
}

