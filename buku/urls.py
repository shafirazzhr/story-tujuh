from django.urls import path

from . import views

app_name = 'buku'

urlpatterns = [
    path('buku', views.buku, name='buku'),
    path('data/', views.data, name='data'),

]
