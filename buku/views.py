from django.shortcuts import render
from django.http import JsonResponse
import requests
import json

# Create your views here.
def buku(request):
    return render (request, 'buku.html')

def data(request):
    try:
        arg = request.GET['q']
    except:
        arg = ""
        
    url = 'https://www.googleapis.com/books/v1/volumes?q=' +arg
    r = requests.get(url)
    data = json.loads(r.content)
    return JsonResponse(data, safe=False)
