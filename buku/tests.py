from django.test import TestCase, Client
from django.urls import reverse, resolve
from .views import buku

class TestBuku(TestCase):
    def test_url_exist(self):
        response = Client().get('/buku')
        self.assertEqual(response.status_code, 200)

    def test_template_used(self):
        response = Client().get('/buku')
        self.assertTemplateUsed(response,'buku.html')

    def test_function_used(self):
        response = Client().get('/buku')
        found = resolve('/buku')
        self.assertEqual(found.func, buku)

    def test_json_url(self):
        response = Client().get('/data/')
        self.assertEqual(response.status_code,200)

