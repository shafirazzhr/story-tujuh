from django.urls import path

from . import views

app_name = 'pengalaman'

urlpatterns = [
    path('pengalaman', views.pengalaman, name='pengalaman'),
]
