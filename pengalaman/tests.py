from django.test import TestCase, Client
from django.urls import reverse, resolve
from .views import pengalaman

class TestPengalaman(TestCase):

    def test_url_exist(self):
        response = Client().get('/pengalaman')
        self.assertEqual(response.status_code, 200)

    def test_template_used(self):
        response = Client().get('/pengalaman')
        self.assertTemplateUsed(response,'pengalaman.html')

    def test_function_used(self):
        response = Client().get('/pengalaman')
        found = resolve('/pengalaman')
        self.assertEqual(found.func, pengalaman)

